#include "Command.h"
#include "Request.h"


void Command::call(const QString& command, const QVariantMap& args, HandlerType handler, const QVariant& label, int timeout_ms)
{
    Request* request = new Request();

    QObject::connect(request, &Request::finished, [handler, label, request](const QVariant& data, ZMessage::RequestError error)
    {
        handler(data, error, label);
        request->deleteLater();
    });

    request->send(ZMessage::prepareRequest(command, args), timeout_ms);
}

void Command::setHost(const QString& targetHost)
{
    Request::HOST = targetHost;
}
