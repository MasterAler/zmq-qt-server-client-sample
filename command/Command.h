#pragma once

#include <QVariantMap>
#include <functional>

#include "ZMessage.h"

namespace Command
{
    // command callback type
    typedef std::function<void(const QVariant&, ZMessage::RequestError, const QVariant)> HandlerType;

    /*!
     * \brief setHost      -- sets target host 4 requests, globally for the client
     * \param targetHost   -- some valid zmq connection string
     */
    void setHost(const QString& targetHost);

    /*!
     * \brief call         -- sends command with args to broker
     * \param command      -- command string
     * \param args         -- map of command arguments
     * \param handler      -- callback to be triggered on response or timeout
     * \param label        -- label to identify the request (optional)
     * \param timeout_ms   -- command timeout
     */
    void call(const QString& command,
              const QVariantMap& args,
              HandlerType handler,
              const QVariant& label = QVariant(),
              int timeout_ms = 5000);
}
