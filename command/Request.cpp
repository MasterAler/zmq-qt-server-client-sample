#include "Request.h"
#include "ZConfig.h"

#include <nzmqt/nzmqt.hpp>

#include <QTimer>
#include <QJsonDocument>
#include <QJsonObject>
#include <QVariantMap>
#include <QFile>

QString Request::HOST = ZConfig().getConnectionString();

/*************************************************************************************************************************/

class RequestPrivate
{
public:
    explicit RequestPrivate(QObject* parent, nzmqt::ZMQContext* zmqContext)
        : m_timeoutTimer(new QTimer(parent))
        , m_ioSocket(zmqContext->createSocket(nzmqt::ZMQSocket::TYP_REQ, parent))
        , m_zmqContext(zmqContext)
    {
        m_timeoutTimer->setSingleShot(true);
        m_ioSocket->setLinger(0);

        try
        {
            m_ioSocket->connectTo(Request::HOST);
        }
        catch (...)
        {
            qWarning() << "failed to connect: " << Request::HOST;
        }
    }

public:
    QTimer*                             m_timeoutTimer;
    nzmqt::ZMQSocket*                   m_ioSocket;
    QScopedPointer<nzmqt::ZMQContext>   m_zmqContext;
};

/*************************************************************************************************************************/

Request::Request(QObject* parent)
    : QObject(parent)
    , d(new RequestPrivate(this, nzmqt::createDefaultContext()))
{
    connect(d->m_timeoutTimer, &QTimer::timeout, [this]
    {
        d->m_ioSocket->close();

        ZMessage::ResponseData timeoutResponse;
        emit finished(timeoutResponse.data, timeoutResponse.error);
    });

    connect(d->m_ioSocket, &nzmqt::ZMQSocket::messageReceived, [this](const QList<QByteArray>& message)
    {
        QByteArray data;
        for (auto piece : message)
            data.append(piece);

        d->m_timeoutTimer->stop();
        ZMessage::ResponseData result = ZMessage::parseResponse(data);

        emit finished(result.data, result.error);
    });
}

Request::~Request()
{
    d->m_zmqContext->stop();
}

void Request::send(const QByteArray& data, int timeout_ms)
{
    try
    {
        /* ZMQ context is started here, because
         * Request class is designed to be used via Command::call,
         * thus resulting it to be a "one-off", disposable class,
         * whose lifetime is equal to a single request execution
         *********************************************************/
        d->m_zmqContext->start();
        d->m_ioSocket->sendMessage(data);
    }
    catch(...)
    {
        qWarning() << "failed to send to: " << Request::HOST;
    }

    d->m_timeoutTimer->start(timeout_ms);
}
