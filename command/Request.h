#pragma once

#include <QObject>
#include <QScopedPointer>

#include "ZMessage.h"

namespace nzmqt
{
    class ZMQContext;
}

class RequestPrivate;

/*!
 * \brief The Request class
 * is the class, performing a client request to the
 * server-side broker. Also provides response data if any exists.
 */
class Request : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Request)

public:
    explicit Request(QObject* parent = nullptr);
    virtual ~Request();

    // Target host is set this way 4 now, should be improved
    static QString HOST;

public slots:
    /*!
     * \brief send          -- (surprise!) sends request data to remote broker
     * \param data          -- data to be sent
     * \param timeout_ms    -- timeout to wait for response
     */
    void send(const QByteArray& data, int timeout_ms = 5000);

signals:
    /*!
     * \brief finished -- is a signal, emitted on
     * response receiving or timeout. Provides the corresponding
     * result data as a map
     */
    void finished(const QVariant& data, ZMessage::RequestError error);

private:
    QScopedPointer<RequestPrivate> d;
};
