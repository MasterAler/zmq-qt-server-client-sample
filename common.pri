QT += network

HEADERS += \
    $$ROOT_DIR/common/ZAppLauncher.h \
    $$ROOT_DIR/common/ZMessage.h \
    $$ROOT_DIR/common/ZConfig.h

SOURCES += \
    $$ROOT_DIR/common/ZAppLauncher.cpp \
    $$ROOT_DIR/common/ZMessage.cpp \
    $$ROOT_DIR/common/ZConfig.cpp

INCLUDEPATH *= $$ROOT_DIR/common
DEPENDPATH *= $$INCLUDEPATH
