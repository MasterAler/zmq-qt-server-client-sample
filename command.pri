QT += network

HEADERS += \
    $$ROOT_DIR/command/Command.h \
    $$ROOT_DIR/command/Request.h

SOURCES += \
    $$ROOT_DIR/command/Command.cpp \
    $$ROOT_DIR/command/Request.cpp

INCLUDEPATH *= $$ROOT_DIR/command
DEPENDPATH *= $$INCLUDEPATH
