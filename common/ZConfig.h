#pragma once

#include <QString>
#include <QVariant>

/*!
 * \brief The ZConfig class
 * is a convenience class, used to
 * read settings from the config files of Zero lib
 */
class ZConfig
{
public:
    explicit ZConfig(const QString& filename = QString())
        : mConfigFilename(filename)
    {}

    /*!
     * \brief getConnectionString -- obtains ZMQ connection string from config
     * \return                    -- connection string if config file exists or default if not
     */
    QString getConnectionString() const;

    /*!
     * \brief getDebugModeIsEnabled -- obtains message logging status, false by default
     * \return                      -- verbose messages on/off, value is read from file or false
     */
    bool getDebugModeIsEnabled() const;

private:
    QString mConfigFilename;

    /*!
     * \brief readConfigValue -- simple wrapper over QSettings with ini-file usage
     * \param key             -- key to read
     * \param defaultValue    -- deafult value is key doesn't exist
     * \return                -- config value by the key
     */
    QVariant readConfigValue(const QString& key, const QVariant& defaultValue) const;
};

