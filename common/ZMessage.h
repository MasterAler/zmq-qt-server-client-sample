#pragma once

#include <QObject>
#include <QVariantMap>
#include <QByteArray>

/*!
 * \brief The ZMessage class
 * is the prime and only class (not struct, because of Qt's meta usage),
 * describing simple JSON API and providing helper methods to support it
 * Should never contain any command details other than general format.
 */
class ZMessage
{
    // Macro contains public/private specifiers, thus it's a class
    Q_GADGET

public:
    /*!
     * \brief The RequestError enum
     * is a string-convertible enum of request/response
     * error statuses
     */
    enum RequestError
    {
        NoError = 0,
        ParseError,
        InvalidSchema,
        CommandNotSupported,
        BadServerReply,
        OperationTimeout
    };
    Q_ENUM(RequestError)

    /*!
     * \brief The RequestData struct
     * describes the way any request is supposed to look like
     */
    struct RequestData
    {
        QString     command;
        QVariantMap args;
        QString     sender;
    };

    /*!
     * \brief The ResponseData struct
     * describes the way any response is supposed to look like
     */
    struct ResponseData
    {
        /*!
         * \brief ResponseData
         * constructs a timeout response by default
         */
        ResponseData()
            : error(OperationTimeout)
        {}

        QVariant     data;
        RequestError error;
    };

    // Request
    static const QString COMMAND_KEY;
    static const QString ARGS_KEY;
    static const QString SENDER_KEY;

    //Response
    static const QString DATA_KEY;
    static const QString ERROR_KEY;

    // ---------------------  *** helpers *** ---------------------

    /*!
     * \brief parseRequest -- parses request binary data into request struct
     * \param requestData  -- binary data, sent by client or something
     * \param error        -- parse error, NoError if succeeds
     * \return             -- request data struct
     */
    static RequestData parseRequest(const QByteArray& requestData, ZMessage::RequestError& error);

    /*!
     * \brief parseResponse -- parses response binary data into response struct
     * \param responseData  -- binary data, sent by server side as response
     * \return              -- ResponseData with data and error status
     */
    static ResponseData parseResponse(const QByteArray& responseData);

    /*!
     * \brief prepareResponse -- helper method, converts QVariant data to response
     * \param error           -- error, if response signalizes about any
     * \param data            -- data to be prepared
     * \return                -- binary data to be sent as response
     */
    static QByteArray prepareResponse(ZMessage::RequestError error, const QVariant& data = QVariant(""));

    /*!
     * \brief prepareRequest -- helper method, converts command data to request
     * \param command        -- command string
     * \param args           -- map of command arguments
     * \return               -- binary data to be sent as request
     */
    static QByteArray prepareRequest(const QString& command, const QVariantMap& args);

    static const QVariantMap TIMEOUT_RESPONSE;
};
