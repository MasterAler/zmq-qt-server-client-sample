#pragma once

#include <QCoreApplication>
#include <functional>

/*!
 * \brief The ZAppLauncher class
 * is a convenience class, used to organize a console app
 * with it's own loop without any threads or complications.
 * Idea was uprightly stolen from nzmqt::NzmqtApp
 */
class ZAppLauncher : public QCoreApplication
{
    Q_OBJECT
public:
    explicit ZAppLauncher(int& argc, char** argv);
    ~ZAppLauncher();

    void setRunner(std::function<void(void)>&& runner);

protected slots:
    void run();

private:
    std::function<void(void)> m_runnerImpl;
};
