#include "ZConfig.h"

#include <QSettings>

namespace
{
    const QString CONN_ADDR_KEY  { "General/addr" };
    const QString DEBUG_MODE_KEY { "General/debug" };
}

QString ZConfig::getConnectionString() const
{
    const QString DEFAULT_CONNECTION_STRING { "tcp://127.0.0.1:7003" };

    QString result = readConfigValue(CONN_ADDR_KEY, DEFAULT_CONNECTION_STRING).toString();
    return result;
}

bool ZConfig::getDebugModeIsEnabled() const
{
    bool result = readConfigValue(DEBUG_MODE_KEY, false).toBool();
    return result;
}

QVariant ZConfig::readConfigValue(const QString& key, const QVariant& defaultValue) const
{
    QVariant result;

    if (!ZConfig::mConfigFilename.isEmpty())
    {
        QSettings config(ZConfig::mConfigFilename, QSettings::IniFormat);
        config.setIniCodec("UTF-8");

        result = config.value(key, defaultValue).toString();
    }
    else
        result = defaultValue;

    return result;
}
