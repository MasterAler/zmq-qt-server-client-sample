#include "ZAppLauncher.h"

#include <QTimer>
#include <QDebug>

#include <stdexcept>

ZAppLauncher::ZAppLauncher(int& argc, char** argv)
    : QCoreApplication(argc, argv)
{
    QTimer::singleShot(0, this, &ZAppLauncher::run);
}

ZAppLauncher::~ZAppLauncher()
{
    qInfo() << "=== Finished ===";
}

void ZAppLauncher::setRunner(std::function<void ()>&& runner)
{
    m_runnerImpl = std::move(runner);
}

void ZAppLauncher::run()
{
    try
    {
        m_runnerImpl();
    }
    catch (std::exception& ex)
    {
        qCritical() << ex.what();
        exit(-1);
    }
}
