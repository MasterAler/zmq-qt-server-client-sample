#include "ZMessage.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QHostInfo>
#include <QDebug>

namespace
{
    bool isRequestValid(const QVariantMap& data)
    {
        return data.contains(ZMessage::COMMAND_KEY) && !data[ZMessage::COMMAND_KEY].toString().isEmpty()
                && data.contains(ZMessage::ARGS_KEY) && data.contains(ZMessage::SENDER_KEY);
    }

    bool isResponseValid(const QVariantMap& data)
    {
        return data.contains(ZMessage::DATA_KEY)  &&
               data.contains(ZMessage::ERROR_KEY) && data[ZMessage::ERROR_KEY].canConvert(QVariant::Int);
    }
}

const QString ZMessage::COMMAND_KEY = QStringLiteral("command");

const QString ZMessage::ARGS_KEY = QStringLiteral("args");
const QString ZMessage::SENDER_KEY = QStringLiteral("sender");

const QString ZMessage::DATA_KEY = QStringLiteral("data");
const QString ZMessage::ERROR_KEY = QStringLiteral("error");

/**************************************************************************************************************/

ZMessage::RequestData ZMessage::parseRequest(const QByteArray &requestData, ZMessage::RequestError& error)
{
    ZMessage::RequestData result;

    QJsonParseError parseError;
    QVariantMap request = QJsonDocument::fromJson(requestData, &parseError).object().toVariantMap();

    if (parseError.error != QJsonParseError::NoError)
    {
        error = ZMessage::ParseError;
        return result;
    }

    if (!isRequestValid(request))
    {
        error = ZMessage::InvalidSchema;
        return result;
    }

    result.command = request[COMMAND_KEY].toString();
    result.args = request[ARGS_KEY].toMap();
    result.sender = request[SENDER_KEY].toString();
    error = ZMessage::NoError;

    return result;
}

ZMessage::ResponseData ZMessage::parseResponse(const QByteArray& responseData)
{
    ResponseData result;

    QJsonParseError parseError;
    QVariantMap response = QJsonDocument::fromJson(responseData, &parseError).object().toVariantMap();

    if (parseError.error != QJsonParseError::NoError)
    {
        qCritical() << parseError.errorString();
        result.error = ZMessage::BadServerReply;
    }
    else if (!isResponseValid(response))
        result.error = ZMessage::InvalidSchema;
    else
    {
        result.error = response[ZMessage::ERROR_KEY].value<ZMessage::RequestError>();
        result.data  = response[ZMessage::DATA_KEY];
    }

    return result;
}

QByteArray ZMessage::prepareResponse(ZMessage::RequestError error, const QVariant& data)
{
    QVariantMap response;

    response[ZMessage::DATA_KEY]  = data;
    response[ZMessage::ERROR_KEY] = error;

    return QJsonDocument(QJsonObject::fromVariantMap(response)).toJson(QJsonDocument::Compact);
}

QByteArray ZMessage::prepareRequest(const QString& command, const QVariantMap& args)
{
    static QString LOCAL_HOST_NAME = QHostInfo::localHostName();

    QVariantMap result;

    result[ZMessage::COMMAND_KEY]   = command;
    result[ZMessage::ARGS_KEY]      = args;
    result[ZMessage::SENDER_KEY]    = LOCAL_HOST_NAME;

    return QJsonDocument(QJsonObject::fromVariantMap(result)).toJson(QJsonDocument::Compact);
}
