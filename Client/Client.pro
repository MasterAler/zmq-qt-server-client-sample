QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

ROOT_DIR = $$PWD/..

CONFIG(release, debug|release) {
  BUILD_DIR = $$ROOT_DIR/bin
}
CONFIG(debug, debug|release) {
  BUILD_DIR = $$ROOT_DIR/bin_debug
}

DESTDIR = $$BUILD_DIR

include($$ROOT_DIR/zmq.pri)
include($$ROOT_DIR/command.pri)
include($$ROOT_DIR/common.pri)

SOURCES += \
        main.cpp

