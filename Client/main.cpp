#include <QCoreApplication>
#include <QDebug>
#include <QTimer>
#include <QEventLoop>
#include <QDateTime>

#include <Command.h>
#include <ZAppLauncher.h>

int main(int argc, char *argv[])
{
    ZAppLauncher launcher(argc, argv);

    auto submitDataHandler = [](const QVariant& reply, ZMessage::RequestError error, const QVariant& label)
    {
        qInfo() << reply;
        qInfo() << QVariant::fromValue(error);
        qInfo() << label;
    };

    launcher.setRunner([submitDataHandler](){
        qInfo() << "== Started Client ==";

        QVariantMap  cmdArgs;
        cmdArgs["some_fkn_number"] = 123;
        cmdArgs["some_fkn_string"] = "asdfasdf";

        const int N = 10;
        for (int i = 0; i < N; ++i)
            Command::call("BEHOLD!", cmdArgs, submitDataHandler, QString("LOL %1").arg(i));

        Command::call("LOLER", cmdArgs, submitDataHandler, "LOL 666");
    });

    return launcher.exec();
}
