ZMQ_DIR = $$ROOT_DIR/zmq

NZMQT_DIR = $$ZMQ_DIR/qt_bind

HEADERS += \
    $$NZMQT_DIR/zmq.hpp \
    $$NZMQT_DIR/nzmqt/global.hpp \
    $$NZMQT_DIR/nzmqt/impl.hpp \
    $$NZMQT_DIR/nzmqt/nzmqt.hpp

INCLUDEPATH *= $$NZMQT_DIR

win32 {

HEADERS += \
    $$ZMQ_DIR/include/zmq.h \
    $$ZMQ_DIR/include/zmq_utils.h \

INCLUDEPATH *= $$ZMQ_DIR/include

CONFIG(release, debug|release):ZMQ_LIB = libzmq-v140-mt-4_3_1
CONFIG(debug, debug|release):ZMQ_LIB = libzmq-v140-mt-gd-4_3_1

LIBS *= \
    -L$$ZMQ_DIR/lib \
    -l$$ZMQ_LIB

zmq_lib.path += $$shell_quote($$DESTDIR)
zmq_lib.files += $$ZMQ_DIR/bin/$$ZMQ_LIB.*
INSTALLS += zmq_lib

}

unix {

LIBS += -lzmq

}

DEPENDPATH *= $$INCLUDEPATH
