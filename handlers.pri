HEADERS += \
    $$ROOT_DIR/handlers/ServerHandlerBase.h \
    $$ROOT_DIR/handlers/Replier.h \
    $$ROOT_DIR/handlers/JsonRequestHandler.h

SOURCES += \
    $$ROOT_DIR/handlers/ServerHandlerBase.cpp \
    $$ROOT_DIR/handlers/Replier.cpp \
    $$ROOT_DIR/handlers/JsonRequestHandler.cpp

INCLUDEPATH *= $$ROOT_DIR/handlers
DEPENDPATH *= $$INCLUDEPATH
