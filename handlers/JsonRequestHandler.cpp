#include "JsonRequestHandler.h"


QByteArray JsonRequestHandler::handleRequestMessage(const ZMessage::RequestData& requestData) const
{
    QByteArray result;

    std::string cmd = requestData.command.toUtf8().constData();
    if (m_commandHandlers.find(cmd) != m_commandHandlers.end())
    {
        result = ZMessage::prepareResponse(ZMessage::NoError, m_commandHandlers[cmd](std::move(requestData.args)));
    }

    return result;
}

bool JsonRequestHandler::hasCommand(const QString& command) const
{
    std::string cmd = command.toUtf8().constData();
    return m_commandHandlers.find(cmd) != m_commandHandlers.end();
}

bool JsonRequestHandler::addCommandHandler(const QString& command, JsonRequestHandler::CommandHandler&& handler)
{
    std::string cmd = command.toUtf8().constData();
    if (m_commandHandlers.find(cmd) != m_commandHandlers.end())
        return false;

    m_commandHandlers.emplace(cmd, std::forward<CommandHandler>(handler));
    return true;
}
