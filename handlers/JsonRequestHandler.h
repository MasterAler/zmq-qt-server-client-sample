#pragma once

#include <QByteArray>

#include <functional>
#include <unordered_map>
#include <string>

#include "ZMessage.h"

/*!
 * \brief The JsonRequestHandlerBase class
 * is a lightweight class, implementing command handler registration.
 * It uses ZMessage, describing API format and only manages callables itself.
 */
class JsonRequestHandler
{
public:
    // handler type
    using CommandHandler = std::function<QVariant(const QVariantMap&)>;

    /*!
     * \brief hasCommand -- checks if handler for the command exists
     * \param command    -- command to be searched for
     * \return           -- true if command has already been registered
     */
    bool hasCommand(const QString& command) const;

    /*!
     * \brief addCommandHandler -- adds handler for the specific command
     * \param command           -- command to be registered
     * \param handler           -- callable, that is set as handler
     * \return
     */
    bool addCommandHandler(const QString& command, CommandHandler&& handler);

    /*!
     * \brief handleRequestMessage -- this methods performs the registered handler if any fits the request
     * \param requestData          -- binary data, containing the request
     * \return                     -- response, already prepared as binary data via helper
     */
    QByteArray handleRequestMessage(const ZMessage::RequestData& requestData) const;

private:
    mutable std::unordered_map<std::string, CommandHandler> m_commandHandlers;
};

