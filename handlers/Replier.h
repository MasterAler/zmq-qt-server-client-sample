#pragma once

#include <QObject>
#include "ServerHandlerBase.h"

#include <functional>

class ReplierPrivate;

/*!
 * \brief The Replier class
 * is a class that handles requests via ZeroMQ,
 * implements REQ-REP interaction pattern, supports
 * string commands and handler addition for them.
 */
class Replier : public ServerHandlerBase
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(Replier)
    Q_DISABLE_COPY(Replier)

public:
    explicit Replier(QObject* parent = nullptr);
    ~Replier() override;

    /*!
     * \brief setBindAddress -- sets some particular bind address to be listened
     * \param bindAddress    -- address to bind to
     */
    void setBindAddress(const QString& bindAddress);

    /*!
     * \brief registerCommandHandler -- registers handler for arbitrary command
     * \param command                -- command string
     * \param handler                -- some callable to handle the command
     */
    void registerCommandHandler(const QString& command, std::function<QVariant(const QVariantMap&)>&& handler);

signals:
    /*!
     * \brief requestReceived -- emitted when request is received
     * \param request         -- request that was received, as byte data
     */
    void requestReceived(const QByteArray& request);

    /*!
     * \brief replySent -- emitted when reply was sent
     * \param reply     -- reply that was sent, as byte data
     */
    void replySent(const QByteArray& reply);

protected:
    void startImpl() override;

private:
    QScopedPointer<ReplierPrivate> d_ptr;
};

