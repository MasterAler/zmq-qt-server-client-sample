#pragma once

#include <QObject>
#include <QSharedPointer>


namespace nzmqt
{
    class ZMQContext;
}

/*!
 * \brief The ServerHandlerBase class
 * is a base class for server-side ZeroMQ broker
 * handlers. Having a base class is possibly excessive, but could be
 * useful in case of different interaction strategies support
 *
 * TODO:
 * If only REQ-REP stays, Replier signals should be moved here or base class
 * is to be removed at all, more generalizations could be done otherwise
 */
class ServerHandlerBase : public QObject
{
    Q_OBJECT

public:
    virtual ~ServerHandlerBase();

public slots:
    /*!
     * \brief start -- starts context and handler implementation
     */
    void start();

    /*!
     * \brief stop -- stops context and handler implementation
     */
    void stop();

signals:
    /*!
     * \brief finished -- emitted (surprise!) when handler is finished
     */
    void finished();

    /*!
     * \brief failure -- emitted on failure
     * \param what    -- error text
     */
    void failure(const QString& what);

protected:
    explicit ServerHandlerBase(QObject* parent = nullptr);

    virtual void startImpl() = 0;

    virtual void stopImpl() {}

    /*!
     * \brief defaultContext -- only one, default context is currently supported,
     * this method returns ptr to it
     * \return -- ptr to the default context
     */
    static QSharedPointer<nzmqt::ZMQContext> defaultContext();
};
