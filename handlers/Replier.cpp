#include "Replier.h"

#include <QList>
#include <QSharedPointer>

#include <nzmqt/nzmqt.hpp>

#include "ZMessage.h"
#include "ZConfig.h"
#include "JsonRequestHandler.h"

namespace
{
    const QString DEFAULT_BIND_ADDRESS = ZConfig().getConnectionString();
}

class ReplierPrivate
{
public:
    ReplierPrivate()
        : m_handler(new JsonRequestHandler)
        , m_bindAddress(DEFAULT_BIND_ADDRESS)
    {}

    QScopedPointer<JsonRequestHandler>      m_handler;

    QString                                 m_bindAddress;
    nzmqt::ZMQSocket*                       m_socket;
};

/********************************************************************************/

Replier::Replier(QObject *parent)
    : ServerHandlerBase(parent)
    , d_ptr(new ReplierPrivate())
{
    Q_D(Replier);

    d->m_socket = ServerHandlerBase::defaultContext()->createSocket(nzmqt::ZMQSocket::TYP_REP, this);
    d->m_socket->setObjectName("Replier.Socket.socket(REP)");

    connect(d->m_socket, &nzmqt::ZMQSocket::messageReceived, [this, d](const QList<QByteArray>& message){
        QByteArray data;
        for (auto const& piece : message)
            data.append(piece);

        emit requestReceived(data);

        QByteArray response;
        ZMessage::RequestError error;
        ZMessage::RequestData requestData = ZMessage::parseRequest(data, error);

        if (ZMessage::NoError == error)
        {
            if (d->m_handler->hasCommand(requestData.command))
            {
                try
                {
                    response = d->m_handler->handleRequestMessage(requestData);
                }
                catch (std::exception& ex)
                {
                    qCritical() << QString("Exception occured while trying to handle %1 command:").arg(requestData.command);
                    qCritical() << ex.what();

                    emit failure(ex.what());
                    emit finished();
                }
            }
            else
                response = ZMessage::prepareResponse(ZMessage::CommandNotSupported);
        }
        else
            response = ZMessage::prepareResponse(error);

        d->m_socket->sendMessage(response);
        emit replySent(response);
    });
}

Replier::~Replier()
{
    QSharedPointer<nzmqt::ZMQContext> context = ServerHandlerBase::defaultContext();
    if (!context->isStopped())
        context->stop();
}

void Replier::setBindAddress(const QString& bindAddress)
{
    Q_D(Replier);
    d->m_bindAddress = bindAddress;
}

void Replier::registerCommandHandler(const QString& command, std::function<QVariant(const QVariantMap&)>&& handler)
{
    Q_D(Replier);
    d->m_handler->addCommandHandler(command, std::forward<JsonRequestHandler::CommandHandler>(handler));
}

void Replier::startImpl()
{
    Q_D(Replier);
    d->m_socket->bindTo(d->m_bindAddress);
}
