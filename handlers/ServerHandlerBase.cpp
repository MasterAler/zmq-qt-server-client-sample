#include "ServerHandlerBase.h"

#include <nzmqt/nzmqt.hpp>

ServerHandlerBase::ServerHandlerBase(QObject* parent)
    : QObject(parent)
{ }

ServerHandlerBase::~ServerHandlerBase()
{ }

QSharedPointer<nzmqt::ZMQContext> ServerHandlerBase::defaultContext()
{
    static QSharedPointer<nzmqt::ZMQContext> context(nzmqt::createDefaultContext());
    return context;
}

void ServerHandlerBase::start()
{
    try
    {
        ServerHandlerBase::defaultContext()->start();
        startImpl();
    }
    catch (const nzmqt::ZMQException& ex)
    {
        emit failure(ex.what());
        emit finished();
    }
}

void ServerHandlerBase::stop()
{
    ServerHandlerBase::defaultContext()->stop();
    stopImpl();

    emit finished();
}
