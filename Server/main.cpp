#include <QCoreApplication>
#include <QDebug>
#include <QVariantMap>

#include <Replier.h>
#include <ZAppLauncher.h>

#include <functional>

int main(int argc, char *argv[])
{
    ZAppLauncher launcher(argc, argv);

    QScopedPointer<Replier> handler(new Replier);
    QObject::connect(handler.data(), &Replier::finished, &launcher, &ZAppLauncher::quit);

    Replier* replier = qobject_cast<Replier*>(handler.data());

    QObject::connect(handler.data(), &Replier::requestReceived, [](const QByteArray& request){
        qInfo() << request;
    });

    QObject::connect(handler.data(), &Replier::replySent, [](const QByteArray& reply) {
        qWarning() << reply;
    });

    replier->registerCommandHandler("BEHOLD!", [](const QVariantMap&) -> QVariant {
            QVariantMap resp_data;
            resp_data["LOL"] = "ok";
            resp_data["AZAZ"] = 827;

            return resp_data;
    });

    launcher.setRunner([&handler](){
        qInfo() << "=== Started Server ===";
        handler->start();
    });

    return launcher.exec();
}
